package com.devcamp.j62crudfrontendrebuilt.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.j62crudfrontendrebuilt.model.CVoucher;
import com.devcamp.j62crudfrontendrebuilt.repository.IVoucherRepository;

@Service
public class CVoucherService {
    @Autowired
	IVoucherRepository pVoucherRepository;
    public ArrayList<CVoucher> getVoucherList() {
        ArrayList<CVoucher> listCVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(listCVoucher::add);
        return listCVoucher;
    }
}
